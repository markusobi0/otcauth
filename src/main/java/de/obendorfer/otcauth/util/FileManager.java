package de.obendorfer.otcauth.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileManager {

  public static boolean isAvailable(String fileName) {
    boolean result = false;
    File file = new File(fileName);
    if (file.exists()) {
      result = true;
    }
    return result;
  }

  public static HashMap<String, String> readFile(String fileName) throws IOException {
    HashMap<String, String> result = new HashMap<>();
    Path filePath = Paths.get(fileName);
    System.out.println(filePath.toAbsolutePath());
    List<String> lines = Files.readAllLines(filePath);
    for (String line : lines) {
      String[] data = line.split(": ");
      result.put(data[0], data[1]);
    }

    return result;
  }

  public static void writeFile(String fileName, HashMap<String, String> data) throws IOException {
    List<String> lines = new ArrayList<>();
    for (Map.Entry<String, String> entry : data.entrySet()) {
      lines.add(entry.getKey() + ": " + entry.getValue() + "\n");
    }
    File file = new File(fileName);
    file.createNewFile();
    FileWriter writer = new FileWriter(fileName);

    writer.write(lines.get(0));
    lines.remove(0);
    for (String line : lines) {
      writer.append(line);
    }
    writer.flush();
    writer.close();
  }

  public static void writeFile(String fileName, String data) throws IOException {
    List<String> lines = new ArrayList<>();
    lines.add(data);
    File file = new File(fileName);
    file.createNewFile();
    FileWriter writer = new FileWriter(fileName);

    writer.write(lines.get(0));
    lines.remove(0);
    for (String line : lines) {
      writer.append(line);
    }
    writer.flush();
    writer.close();
  }

  public static void deleteFile(String fileName) {
    File file = new File(fileName);
    if (fileName.contains(".txt")) {
      file.delete();
    }
  }
}
