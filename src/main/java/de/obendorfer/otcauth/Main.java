package de.obendorfer.otcauth;

import com.huawei.openstack4j.api.OSClient.OSClientV3;
import com.huawei.openstack4j.model.common.Identifier;
import com.huawei.openstack4j.model.identity.v3.Token;
import com.huawei.openstack4j.openstack.OSFactory;
import de.obendorfer.otcauth.util.FileManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

  private static final String IAM_ENDPOINT = "https://iam.eu-de.otc.t-systems.com/v3";
  public static HashMap<String, String> loginData;
  public static HashMap<String, String> tokenData = new HashMap<>();
  private static String dataStore = "dataStore.txt";
  private static String tokenStore = "tokenStore.txt";
  private static String tokenFile = "token.txt";
  private static Token token = null;

  public static void main(String[] args) {
    if (FileManager.isAvailable(dataStore)) {
      try {
        System.out.println("\n* Using presaved data ...");
        loginData = FileManager.readFile(dataStore);
      } catch (IOException e) {
      }
    } else {
      inputData();
    }

    try {
      System.out.println("\n* Trying to log in ...");
      token = generateToken();
      System.out.println("\n* Auth generating ...");
      printAuthfile();
    } catch (Exception e) {
      System.out.println("\n* Auth failed! exiting ...");
      FileManager.deleteFile(dataStore);
    }
  }

  private static void printAuthfile() {
    System.out.println("\n* Writing Authfile ...\n");
    tokenData.put("Issued At", token.getIssuedAt().toString());
    tokenData.put("Expires", token.getExpires().toString());
    tokenData.put("Tenant-ID", loginData.get("tenantId"));
    tokenData.put("Username", loginData.get("username"));
    tokenData.put("Token", token.getId());
    try {
      FileManager.writeFile(tokenStore, tokenData);
      FileManager.writeFile(tokenFile, tokenData.get("Token"));
    } catch (IOException e) {
      e.printStackTrace();
    }

    for (Map.Entry<String, String> entry : tokenData.entrySet()) {
      System.out.println(entry.getKey() + ": " + entry.getValue());
    }
    System.out.println("* Generated Token located in " + tokenFile);
  }

  private static void inputData() {
    loginData = new HashMap<>();
    Scanner scan = new Scanner(System.in);
    System.out.print("Tenant-ID: ");
    loginData.put("tenantId", scan.next());

    System.out.print("Project-Name: ");
    loginData.put("projectName", scan.next());

    System.out.print("Username: ");
    loginData.put("username", scan.next());

    System.out.print("Password: ");
    loginData.put("password", scan.next());

    System.out.print("Should the information be stored? (Y/N): ");
    if (scan.next().toLowerCase().contains("y") || scan.next().toLowerCase().contains("j")) {
      try {
        FileManager.writeFile(dataStore, loginData);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    scan.close();
  }

  private static Token generateToken() {
    Identifier domainIdentifier = Identifier.byName(loginData.get("tenantId"));
    Identifier projectIdentifier = Identifier.byName(loginData.get("projectName"));
    OSClientV3 os = OSFactory.builderV3()
            .endpoint(IAM_ENDPOINT)
            .credentials(loginData.get("username"), loginData.get("password"), domainIdentifier)
            .scopeToProject(projectIdentifier)
            .authenticate();
    Token token = os.getToken();
    return token;
  }
}
